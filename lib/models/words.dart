import 'character_test.dart';

class WordList {

  static const Map<String, List<String>> _elements = {
    Element.Agua: [
      "rejeição", "razão", "isolamento", "improvisação", "criação",
      "inovação", "lógica", "genialidade", "invenção",
      "outra água", "desleixo", "desorganização", "explicação", "displicência",
      "reservado", "repulsa", "recusa", "conhecimento", "exotismo",
      "descoberta", "indecisão", "dúvidas", "criatividade", "incertezas"
    ],
    Element.Terra: [
      "dependência", "abandono", "carinho", "carência", "insatisfação",
      "oratória", "desamparo", "afeto", "apego", "retórica", "solidariedade",
      "sensibilidade", "romantismo", "fama", "locução", "insaciável",
      "contato", "acolhimento", "narração", "ser amado", "fome compulsiva",
      "tagarela", "reconhecimento", "mimo"
    ],
    Element.Madeira: [
      "briga", "negociação", "vendas", "persuasão", "convencimento",
      "liderança", "manipulação", "comando", "confronto", "empreender",
      "confronto", "combate", "atrito", "vantagem", "execução", "direção",
      "conflito", "desavença", "foco", "produtividade", "enrolação",
      "mania de perseguição", "determinação", "ação"
    ],
    Element.Metal: [
      "submissão", "técnica", "proteção", "humilhação", "pessimismo",
      "mágoa", "tristeza",
      "organização", "prevenção", "método", "privacidade", "critérios",
      "análise", "ponderação", "isolamento", "segredo", "comedimento",
      "sacrifício", "paciência", "martírio",
      "intimidade", "provação", "particularidade", "preocupação"
    ],
    Element.Fogo: [
      "narcisismo", "traição", "perfeccionismo", "sofisticação",
      "sedução", "competição", "conquista", "vaidade", "superficialidade",
      "exigência", "sex appeal", "sucesso", "concorrência", "rivalidade",
      "detalhes", "competência", "elogio", "atração", "centro das atenções",
      "exclusão", "ciúmes", "desconfiança", "impecabilidade", "disputa"
    ]
  };

  List<String> _wordList = [
    ..._elements[Element.Agua], ..._elements[Element.Terra],
    ..._elements[Element.Madeira], ..._elements[Element.Metal],
    ..._elements[Element.Fogo]
  ]..shuffle();

  get words => _wordList;

  get elements => _elements;

}