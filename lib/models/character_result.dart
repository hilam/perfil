import 'package:perfil/models/elements.dart';

class CharacterResult {
  Map<String, int> _results;

  CharacterResult() {
    _results = {
      CharacterTest.Agua: 0,
      CharacterTest.Terra: 0,
      CharacterTest.Madeira: 0,
      CharacterTest.Metal: 0,
      CharacterTest.Fogo: 0
    };
  }

  get results => _results;

  List<int> values() => results.values.toList();

  void incrementResult(String key) {
    results[key] += 1;
  }

  void decrementResult(String key) {
    results[key] -= 1;
  }



}