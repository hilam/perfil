import 'package:flutter/material.dart';

BoxDecoration backgroundImage() {
    return BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/img/fundo.png'),
          fit: BoxFit.fill,
        )
    );
}