import 'package:flutter/material.dart';
import 'package:perfil/models/character_result.dart';
import 'package:perfil/models/elements.dart';
import 'background_image.dart';
import 'result_page.dart';

class QuestionsPage extends StatefulWidget {
  final CharacterResult characterResult;

  const QuestionsPage({Key key, this.characterResult}) : super(key: key);

  _QuestionsPageState createState() => _QuestionsPageState();
}

class _QuestionsPageState extends State<QuestionsPage> {
  var _questionList = CharacterTest.questionList;
  var _questions = CharacterTest.questions;
  List<bool> _selected = List.generate(CharacterTest.questionListSize, (i) => false);

  _onSelect(int index) {
    setState(() {
      _selected[index] = !_selected[index];
      CharacterTest.elements.forEach((element) {
        if (_selected[index] &&
            _questions[element].contains(_questionList[index])) {
          widget.characterResult.incrementResult(element);
        }

        if (!_selected[index] &&
            _questions[element].contains(_questionList[index])) {
          widget.characterResult.decrementResult(element);
        }
      });
    });
  }

  Widget questionList(BuildContext context) {
    return ListView.builder(
        itemCount: _questionList.length,
        itemBuilder: (_, index) {
          return Card(
            margin: EdgeInsets.all(16.0),
            elevation: 2,
            shape:
                BeveledRectangleBorder(borderRadius: BorderRadius.circular(2)),
            child: Ink(
              color: _selected[index] ? Colors.blueAccent : null,
              child: ListTile(
                title: Text(
                  '${_questionList[index]}',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
                onTap: () => _onSelect(index),
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: backgroundImage(),
        ),
        Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                color: Colors.amberAccent,
                padding: EdgeInsets.all(8.0),
                margin: EdgeInsets.only(top: 25.0),
                child: Text(
                  'Das questões a seguir, toque nas que você considera verdadeiras para você:',
                  style: TextStyle(
                    fontSize: 18,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Expanded(
              child: questionList(context),
            ),
            RaisedButton(
              color: Colors.amberAccent,
              child: Text("Ver Resultados"),
//              textColor: Colors.white,
              onPressed: () {
//                print(wordsResult);
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ResultPage(
                      characterResult: widget.characterResult,
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ],
    );
  }
}
