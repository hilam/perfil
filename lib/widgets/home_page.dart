import 'package:flutter/material.dart';
import 'package:perfil/widgets/explain_page.dart';
import 'package:perfil/widgets/animated_image.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        margin: EdgeInsets.symmetric(vertical: 10),
        child: AnimatedImage(
          imageName: 'assets/img/home.png',
          nextPage: ExplainPage(),
        ),
      ),
    );
  }
}
