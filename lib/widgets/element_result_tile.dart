import 'package:flutter/material.dart';

class ElementResultTile extends StatelessWidget {

  final IconData icon;
  final String name;
  final String number;
  final bool percentil;

  const ElementResultTile(
      {Key key, this.icon, this.name, this.number, this.percentil})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(this.icon),
      title: Text(
        this.name,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 18,
        ),
      ),
      trailing: Text(
        '$number''${percentil ? '%': ''}',
      ),
    );
  }
}
