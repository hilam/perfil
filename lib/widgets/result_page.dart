import 'package:flutter/material.dart';
import 'package:perfil/models/character_result.dart';
import 'package:perfil/models/elements.dart';

import 'background_image.dart';
import 'element_result_tile.dart';

class ResultPage extends StatelessWidget {
  final CharacterResult characterResult;

  const ResultPage({Key key, this.characterResult}) : super(key: key);


  double percentil(int facts) {
    return facts / CharacterTest.facts * 100;
  }

  @override
  Widget build(BuildContext context) {


    var _keys = CharacterTest.elements;
    var _values = this.characterResult.values();

    List<IconData> _icons = [
      Icons.cloud,
      Icons.broken_image,
      Icons.border_color,
      Icons.attachment,
      Icons.autorenew
    ];

    return Stack(
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: backgroundImage(),
        ),
        Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                color: Colors.amberAccent,
                padding: EdgeInsets.all(8.0),
                margin: EdgeInsets.only(top: 25.0),
                child: Text(
                  'Resultado do teste',
                  style: TextStyle(
                    fontSize: 18,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                padding: EdgeInsets.all(16.0),
                itemCount: _keys.length,
                itemBuilder: (_, index) {
                  return Card(
                    elevation: 2,
                    shape: BeveledRectangleBorder(
                        borderRadius: BorderRadius.circular(2)),
                    child: ElementResultTile(
                      icon: _icons[index],
                      name: _keys[index],
                      number: percentil(_values[index]).toStringAsFixed(2),
                      percentil: true,
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ],
    );
  }
}
