import 'package:flutter/material.dart';
import 'package:perfil/models/character_result.dart';
import 'package:perfil/models/elements.dart';
import 'background_image.dart';
import 'questions_page.dart';

class WordsPage extends StatefulWidget {
  _WordsPageState createState() => _WordsPageState();
}

class _WordsPageState extends State<WordsPage> {
  CharacterResult characterResult = new CharacterResult();

  var _wordList = CharacterTest.wordList;
  var _elements = CharacterTest.words;
  var _selected = List.generate(CharacterTest.wordListSize, (i) => false);

//  Map<String, int>  = characterTest.results;

  _onSelect(int index) {
    setState(() {
      _selected[index] = !_selected[index];
      CharacterTest.elements.forEach((element) {
        if (_selected[index] && _elements[element].contains(_wordList[index])) {
          characterResult.incrementResult(element);
        }

        if (!_selected[index] &&
            _elements[element].contains(_wordList[index])) {
          characterResult.decrementResult(element);
        }
      });
    });
  }

  Widget wordsList(BuildContext context) {
    return ListView.builder(
        itemCount: _wordList.length,
        itemBuilder: (_, index) {
          return Card(
            margin: EdgeInsets.all(16.0),
            elevation: 2,
            shape:
                BeveledRectangleBorder(borderRadius: BorderRadius.circular(2)),
            child: Ink(
              color: _selected[index] ? Colors.amberAccent : null,
              child: ListTile(
                title: Text(
                  '${_wordList[index]}',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
                onTap: () => _onSelect(index),
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: backgroundImage(),
        ),
        Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                color: Colors.amberAccent,
                padding: EdgeInsets.all(8.0),
                margin: EdgeInsets.only(top: 25.0),
                child: Text(
                  'Das palavras a seguir, toque nas que tem alguma identificação com você:',
                  style: TextStyle(
                    fontSize: 18,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Expanded(
              child: wordsList(context),
            ),
            RaisedButton(
              color: Colors.amberAccent,
              child: Text("Próxima Fase"),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        QuestionsPage(characterResult: characterResult),
                  ),
                );
              },
            ),
          ],
        ),
      ],
    );
  }
}
