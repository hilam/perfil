import 'package:flutter/material.dart';
import 'package:perfil/widgets/animated_image.dart';
import 'package:perfil/widgets/words_page.dart';

class ExplainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        margin: EdgeInsets.symmetric(vertical: 10),
        child: AnimatedImage(
          imageName: 'assets/img/home1.png',
          nextPage: WordsPage(),
        ),
      ),
    );
  }
}
