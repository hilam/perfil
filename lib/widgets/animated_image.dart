import 'package:flutter/material.dart';

class AnimatedImage extends StatefulWidget {
  final String imageName;
  final Widget nextPage;

  const AnimatedImage({Key key, this.imageName, this.nextPage})
      : super(key: key);

  @override
  _AnimatedImageState createState() => _AnimatedImageState();
}

class _AnimatedImageState extends State<AnimatedImage> {
  double _opacity = 1.0;

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      duration: Duration(seconds: 5),
      opacity: _opacity,
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage(widget.imageName),
          fit: BoxFit.fill,
        )),
        child: InkWell(
          onTap: () {
            setState(() {
              _opacity = 0.0;
            });
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => widget.nextPage),
            );
          },
        ),
      ),
    );
  }
}
